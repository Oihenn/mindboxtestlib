﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace MindboxClient
{
    class MessageReceiver
    {
        static string message;
        static Regex circle = new Regex(@"^\d+$");
        static Regex triangle = new Regex(@"^\d+,\d+,\d+$");
        public static void Receive()
        {
            Console.WriteLine("\n\rПлощадь какой фигуры вы хоите вычислить?\n\r 1. Круг \n\r 2. Треугольник");
            message = Console.ReadLine();
            switch (message)
            {
                case "1":
                    {
                        Console.WriteLine("\n\rВведите радиус круга:");
                        message = Console.ReadLine();
                        if (circle.IsMatch(message))
                        {
                            Console.WriteLine("\n\rПлощадь круга равен: " + MindboxLib.MathLib.DoCircleArea(Double.Parse(message)));
                            break;
                        }
                        else
                        {
                            Console.WriteLine("\n\rНеверные данные.");
                            break;
                        }
                    }
                case "2":
                    {
                        Console.WriteLine("\n\rВведите стороны треугоьника через запятую:");
                        message = Console.ReadLine();
                        if (triangle.IsMatch(message))
                        {
                            string[] messageArr = message.Split(',');
                            Console.WriteLine("\n\rПлощадь треугольника равна: " + MindboxLib.MathLib.DoTriangleArea(Double.Parse(messageArr[0]), Double.Parse(messageArr[1]), Double.Parse(messageArr[2])));
                            break;
                        }
                        else
                        {
                            Console.WriteLine("\n\rНеверные данные.");
                            break;
                        }
                    }
                default:
                    {
                        Console.WriteLine("\n\rНеверные данные.");
                        break;
                    }
            }

            Receive();
        }
    }
}
