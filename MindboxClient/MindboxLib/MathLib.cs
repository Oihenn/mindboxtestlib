﻿using System;

namespace MindboxLib
{
    public class MathLib
    {
        const double Pi = 3.14159265359;
        public static double DoTriangleArea(double a, double b, double c)
        {
            double halfOfPerimeter = (a + b + c) / 2;
            return Math.Sqrt(halfOfPerimeter * (halfOfPerimeter - a) * (halfOfPerimeter - b) * (halfOfPerimeter - c));
        }

        public static double DoCircleArea(double r)
        {
            return Pi * (r * r);
        }
    }
}
